import React, {Component} from 'react';
import Counter from './Counter';

class Counters extends Component {
	render() {
		const {onDecrement, onIncrement, onReset, onDelete, counters} = this.props;
		return (
			<React.Fragment>
				<div className="App">
					<button className="btn btn-success" onClick={onReset}>
						Reset
					</button>
				</div>
				<div>
					{counters.map((counter) => (
						<Counter
							counter={counter}
							key={counter.id}
							value={counter.value}
							onDelete={() => onDelete(counter)}
							onIncrement={() => onIncrement(counter)}
							onDecrement={() => onDecrement(counter)}
						/>
					))}
				</div>
			</React.Fragment>
		);
	}
}

export default Counters;
