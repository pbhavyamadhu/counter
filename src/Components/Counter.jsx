import React from 'react';
import {Component} from 'react';

class Counter extends Component {
	formatCount() {
		const {value} = this.props;
		return value === 0 ? 'Zero' : value;
	}

	getClasses() {
		let classes = 'btn m-3 btn-';
		classes += this.props.value === 0 ? 'warning' : 'success';
		return classes;
	}

	render() {
		return (
			<React.Fragment>
				<div className="row">
					<div className="col-1">
						<h1 className={this.getClasses()}>{this.formatCount()}</h1>
					</div>
					<div className="col">
						<button
							className="btn btn-secondary m-3"
							onClick={this.props.onIncrement}
						>
							+
						</button>
						<button
							className="btn btn-secondary m-3"
							onClick={this.props.onDecrement}
							disabled={this.props.counter.value === 0 ? 'disabled' : ''}
						>
							-
						</button>
						<button
							className="btn btn-danger"
							onClick={() => this.props.onDelete()}
						>
							Delete
						</button>
						<hr></hr>
					</div>
				</div>
			</React.Fragment>
		);
	}
}

export default Counter;
