import React from 'react';

const NavBar = ({totalCounters}) => {
	return (
		<React.Fragment>
			<nav className="navbar navbar-light bg-light">
				<a className="navbar-brand" href="#">
					NavBar{' '}
					<span className="badge rounded-pill bg-primary">{totalCounters}</span>
				</a>
			</nav>
		</React.Fragment>
	);
};

export default NavBar;
